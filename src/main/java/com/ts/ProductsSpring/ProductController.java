package com.ts.ProductsSpring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDAO;
import com.model.Product;




@RestController
public class ProductController {
	//Dependency Injection
	@Autowired
	ProductDAO productDAO;
	
	@RequestMapping("getProducts")
	public List<Product> getAllProducts() {
		return productDAO.getAllProducts();
	}
	@RequestMapping("getProductById/{ID}")
	public Product getProductById(@PathVariable("ID") int Id) {
		return productDAO.getProductById(Id);
	}
	@RequestMapping("getProductByName/{NAME}")
	public Product getProductByName(@PathVariable("NAME") String name) {
		return productDAO.getProductByName(name);
	}
	@PostMapping("registerProduct")
	public Product registerProduct(@RequestBody Product product) {
		return productDAO.registerProduct(product);
	}
	@PutMapping("updateProduct")
	public Product updateProduct(@RequestBody Product product) {
		return productDAO.updateProduct(product);
	}
	@DeleteMapping("deleteProductById/{ID}")
	public String deleteProductById(@PathVariable("ID") int Id) {
		productDAO.delProductById(Id);
		return "deleted successfully";
		//return productDAO.delProductById(Id);
	}
	@RequestMapping("/showProduct")
	public Product showProduct() {
		Product product=new Product(101,"OnePlusNord",100000);
		
		return product;
		
		
	}
	@RequestMapping("/getAllProducts")
	public List<Product> showProducts() {
		List<Product> product=new ArrayList<>();
		Product product1=new Product(101,"OnePlusNord",35000);
		Product product2 = new Product(102, "Vivo", 17000);
        Product product3 = new Product(103, "Redmi", 15000);
        product.add(product1);
        product.add(product2);
        product.add(product3);

        return product;
		
		
	}
}
