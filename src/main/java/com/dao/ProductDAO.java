package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
	public class ProductDAO {
		
		//Dependency Injection
		@Autowired
		ProductRepository productRepository;
		
		public List<Product> getAllProducts() {
			return productRepository.findAll();
		}
		public Product getProductById(int Id){
			Product prod=new Product(1,"Sample Product",99999.99);
			return productRepository.findById(Id).orElse(prod);
			
		}
		public Product getProductByName(String name){
			//Product prod=new Product(1,"Sample Product",99999.99);
			return productRepository.findByName(name);
		}
		public Product registerProduct(Product product) {
			return productRepository.save(product);
		}
		public Product updateProduct(Product product) {
			return productRepository.save(product);
		}
		public void delProductById(int Id){
			 productRepository.deleteById(Id);
			
		}

	}